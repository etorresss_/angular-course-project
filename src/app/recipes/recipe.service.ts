import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { Subject } from 'rxjs';
import { Store } from '@ngrx/store';

import * as ShoppingListActions from './../shopping-list/store/shopping-list.actions';
import * as fromShoppingList from './../shopping-list/store/shopping-list.reducer';

@Injectable({
  providedIn: 'root',
})
export class RecipeService {
  /*private recipes: Recipe[] = [
    new Recipe('Burguer', 'Best burger recipe',
      'https://sevilla.abc.es/contenidopromocionado/wp-content/uploads/sites/2/2019/09/portada-wp-burguer.jpeg',
      [
        new Ingredient('Bread', 2),
        new Ingredient('Beef patty', 1),
        new Ingredient('Lettuce sheets', 3),
        new Ingredient('Tomato slices', 2),
        new Ingredient('Ketchup', 1),
        new Ingredient('Mayo', 1)
      ]),
    new Recipe('Pizza', 'Best homemade recipe',
      'https://i.pinimg.com/originals/91/ae/a0/91aea0bf35422cb317450e0b496c8179.jpg',
      [
        new Ingredient('Pizza dough', 1),
        new Ingredient('Tomato sauce', 1),
        new Ingredient('Jam', 5),
        new Ingredient('Cheese', 10),
        new Ingredient('Mushrooms', 50)
      ])
  ];*/
  private recipes: Recipe[] = [];

  recipeSelected = new Subject<Recipe>();
  recipesChanged = new Subject<Recipe[]>();

  constructor(private store: Store<fromShoppingList.AppState>) {}

  getRecipes(): Recipe[] {
    return this.recipes.slice();
  }

  setRecipes(recipes: Recipe[]): void {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes);
  }

  getRecipe(index: number): Recipe {
    return this.recipes[index];
  }

  sendToShoppingList(ingredients: Ingredient[]): void {
    // this.shoppingListService.addIngredients(ingredients);
    this.store.dispatch(new ShoppingListActions.AddIngredients(ingredients));
  }

  addRecipe(recipe: Recipe): number {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
    return this.recipes.length - 1;
  }

  updateRecipe(index: number, recipe: Recipe): void {
    this.recipes[index] = recipe;
    this.recipesChanged.next(this.recipes.slice());
  }

  deleteRecipe(index: number): void {
    this.recipes.splice(index, 1);
    this.recipesChanged.next(this.recipes.slice());
  }
}
